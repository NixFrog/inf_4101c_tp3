CC=clang
CFLAGS=-Wall -std=c99 -march=native -mtune=native -pg
DEPS = 
OBJ = main.o 

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

cacheTest: $(OBJ)
	gcc -o $@ $^ $(CFLAGS)

clean:
	rm -f *.o *~ cacheTest