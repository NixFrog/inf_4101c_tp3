#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/times.h>
#include <unistd.h>

#define NBLIN 1000
#define NBCOL 1000

typedef struct tms sTms;
typedef struct{
	int begin, end;
	sTms sBegin, sEnd;
}execTime;

void printTime(execTime *tmps){
    printf("Real time:\t %d\nUser Time:\t %ld\nSystem time:\t %ld\n",
		tmps->end - tmps->begin,
		tmps->sEnd.tms_utime - tmps->sBegin.tms_utime,
		tmps->sEnd.tms_stime - tmps->sBegin.tms_stime);
}

void printTab(int **tab, int L, int C){
	int *tab0 = &tab[0][0];
	for (int i = 0 ; i < C*L ; i++){
		printf("%d ",*(tab0));
		if(i%L==0){
			printf("\n");
		}
		tab0++;
	}
	printf("\n");
}

int **badAlloc(int nbLin, int nbCol){
	int **tab; 
	tab = (int **)malloc ( sizeof(int *)  *  nbLin);
	for (int i = 0 ; i < nbLin ; i++){
		tab[i] = (int *)malloc (sizeof(int) * nbCol);
	}

	return tab;
}

void freeBadTable(int nbLin, int **tab){
	for (int i = 0 ; i < nbLin ; i++){
		free (tab[i]);
	}
	free (tab);
}

int **goodAlloc(int nbLin, int nbCol){
	int **tab = (int **)malloc(sizeof(int*)*nbLin);
	int *tab2 = (int *)malloc(sizeof(int)*nbCol*nbLin);
	for(int i = 0 ; i < nbLin ; i++){
		tab[i] = &tab2[i*nbCol];
	}
	return tab;
}
 
void freeGoodTable(int **tab){
	free(tab[0]);
	free(tab);
}

int **oneAlloc(int nbLin, int nbCol){
	int* data;
	int i;
	int **tab = (int**) malloc(nbLin*nbCol*sizeof(int) + nbLin*sizeof(int*));

	for(i=0, data=(int*)(tab+nbLin); i<nbLin; i++,data+=nbCol){
		tab[i] = data;
	}
	return tab;
}

void freeOneAlloc(int **tab){
	free(tab);
}

void initLine(int **tabA, int L, int C, int **tabB){
	for (int i = 0 ; i < C ; i++){
		for (int j = 0 ; j < L ; j++){
			tabA[i][j] = 1;
		}
	}
	for (int i = 0 ; i < C ; i++){
		for (int j = 0 ; j < L ; j++){
			tabB[i][j] = tabA[i][j];
		}
	}
}
//same
void initCol(int **tabA, int L, int C, int **tabB){
	for (int i = 0 ; i < L ; i++){
		for (int j = 0 ; j < C ; j++){
			tabA[i][j] = 1;
		}
	}
	for (int i = 0 ; i < L ; i++){
		for (int j = 0 ; j < C ; j++){
			tabB[i][j] = tabA[i][j];
		}
	
	}
}

//best
void initConti(int **tabA, int L, int C, int **tabB){
	for (int i = 0 ; i < C*L ; i++){
		tabA[0][i] = 1;
	}
	for (int i = 0 ; i < C*L ; i++){
		tabB[0][i] = tabA[0][1];
	}
	// for(int i = 0; i<L; i++){
	// 	memcpy(tabB[i], tabA[i], C*sizeof(tabA[0]));
	// }
}

//worse
void smartInitConti(int **tabA, int L, int C, int **tabB){
	int *tabA0 = &tabA[0][0];
	int *tabB0 = &tabB[0][0];
	for (int i = 0 ; i < C*L ; i++){
		*(tabA0) = 1;
		tabA0++;
	}

	tabA0 = &tabA[0][0];
	for (int i = 0 ; i < C*L ; i++){
		*(tabB0) = *(tabA0);
		tabA0++;
		tabB0++;
	}	
}

//even worse
void smartInitSmartCopy(int **tabA, int L, int C, int **tabB){
	int *tabA0 = &tabA[0][0];
	for (int i = 0 ; i < C*L ; i++){
		*(tabA0) = 1;
		tabA0++;
	}

	for(int i = 0; i<L; i++){
		memcpy(tabB[i], tabA[i], C*sizeof(tabA[0]));
	}
}

int main(){
	///// ** TIME MEASUREMENT ** /////
	// execTime time;
	// int **A, **B, **C;
	// time.begin = times(&time.sBegin);
	// 	for(int i=0; i<50; i++){
	// 		A = badAlloc(NBLIN, NBCOL);
	// 	}
	// time.end = times(&time.sEnd);
	// printTime(&time);

	// time.begin = times(&time.sBegin);
	// 	for(int i=0; i<50; i++){
	// 		B = goodAlloc(NBLIN, NBCOL);
	// 	}
	// time.end = times(&time.sEnd);
	// printTime(&time);

	// time.begin = times(&time.sBegin);
	// 	for(int i=0; i<50; i++){
	// 		C = oneAlloc(NBLIN, NBCOL);
	// 	}
	// time.end = times(&time.sEnd);
	// printTime(&time);
	/////////////////////////////////

	///////// ** RESULTS ** /////////
	// int **A = goodAlloc(NBLIN, NBCOL);
	// int **B = goodAlloc(NBLIN, NBCOL);
	// initConti(A, NBLIN, NBCOL, B);
	
	////// ** CHECKS ** ///////
	// printf("\ncontents of A:\n");
	// printTab(A, NBLIN, NBCOL);
	// printf("\ncontents of B:\n");
	// printTab(B, NBLIN, NBCOL);
	// printf("\ncontents of C:\n");
	// printTab(C, NBLIN, NBCOL);
	
	return 0;
}